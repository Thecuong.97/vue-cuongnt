var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!'
    }
});

var app2 = new Vue({
    el: '#app-2',
    data: {
        message: 'You loaded this page on ' + new Date().toLocaleString()
    }
});

var app3 = new Vue({
    el: '#app-3',
    data: {
        seen: true,
    }
})

var app4 = new Vue({
    el: '#app-4',
    data: {
        todos: [
            { text: 'Learn JavaScript' },
            { text: 'Learn Vue' },
            { text: 'Build something awesome' }
        ]
    }
})

var app5 = new Vue({
    el: '#app-5',
    data: {
        message: 'Ngo The Cuong'
    },
    methods: {
        reverseMessage: function () {
            this.message = this.message.split('').reverse().join('')
        }
    }
})

var app6 = new Vue({
    el: '#app-6',
    data: {
        message: 'Live nội dung nhập vào'
    }
})

Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
})

var app7 = new Vue({
    el: '#app-7',
    data: {
        groceryList: [
            { id: 0, text: 'Nike' },
            { id: 1, text: 'Adidas' },
            { id: 2, text: 'Balenciaga' },
            { id: 3, text: 'Brand' }
        ]
    }
})

var app8 = new Vue({
    el: '#app-8',
    data: {

        testHTML: '<a href="https://www.facebook.com/csneaker97/">Ngô Thế Cường</a>',
    },
});

var app9 = new Vue({
    el: "#app-9",
    data: {
        dem: 0
    },
    methods: {
        tangGiaTri: function () {
            this.dem++;
        }
    }
});

var app10 = new Vue({
    el: "#app-10",
    data: {
        x: 0,
        y: 0
    },
    methods: {
        tinhToaDo: function (event) {
            this.x = event.clientX;
            this.y = event.clientY;
        }
    }
});

var app11 = new Vue({
    el: '#app-11',
    data: {
        message: 'Ngô Thế Cường'
    },
    computed: {
        // một computed getter
        reversedMessage: function () {
            // `this` trỏ tới đối tượng vm
            return this.message.split(' ').reverse().join(' ')
        }
    }
})

var app12 = new Vue({
    el: '#app-12',
    data: {
        message: 'Ngô Thế Cường'
    },
    methods: {
        reverseMessage: function () {
            return this.message.split(' ').reverse().join(' ')
        }
    }
});

var app13 = new Vue({
    el: '#demo',
    data: {
        firstName: 'Ngô Thế',
        lastName: 'Cường'
    },
    computed: {
        fullName: function () {
            return this.firstName + ' ' + this.lastName
        }
    }
});

var app14 = new Vue({
    el: '#app-14',
    data: {
        question: '',
        answer: 'Không thể trả lời nếu bạn chưa đặt câu hỏi!'
    },
    watch: {
        // bất cứ lúc nào câu hỏi thay đổi, hàm bên dưới sẽ chạy
        question: function (newQuestion, oldQuestion) {
            this.answer = 'Đang chờ bạn đặt xong câu hỏi...'
            this.getAnswer()
        }
    },
    methods: {
        // _.debounce là một hàm do Lodash cung cấp
        // Để tìm hiểu rõ hơn cách hoạt động của hàm này,
        // bạn có thể truy cập: https://lodash.com/docs#debounce 
        getAnswer: _.debounce(
            function () {
                if (this.question.indexOf('?') === -1) {
                    this.answer = 'Câu hỏi thì thường chứa một dấu "?" ;-)'
                    return
                }
                this.answer = 'Đang suy nghĩ...'
                var vm = this
                axios.get('https://yesno.wtf/api')
                    .then(function (response) {
                        vm.answer = _.capitalize(response.data.answer)
                    })
                    .catch(function (error) {
                        vm.answer = 'Lỗi! Không thể truy cập API. ' + error
                    })
            },
            // Đây là thời gian (đơn vị mili giây) chúng ta đợi người dùng dừng gõ.
            500
        )
    }
});

var app15 = new Vue({
    el: '#app-15',
    data: {
        isActive: true,
        hasError: false
    }
});

var app16 = new Vue({
    el: '#app-16',
    data: {
        activeClass: 'active',
        errorClass: 'text-danger'
    },

    computed: {
        classObject: function () {
            return {
                active: this.isActive && !this.error,
                'text-danger': this.error && this.error.type === 'fatal'
            }
        }
    }
});

var app17 = new Vue({
    el: '#app-17',
    data: {
        isActive: 'active',
        errorClass: 'text-danger'
    },
});

var app18 = new Vue({
    el: '#app-18',
    data: {
        styleObject: {
            color: 'red',
            fontSize: '13px'
        }
    }
});

var app19 = new Vue({
    el:'#app-19',
    data: {
        xacDinh: true
    }
});

var app20 = new Vue({
    el: '#app-20',
    data: {
        username: true
    }
});

var app21 = new Vue({
    el: '#app-21',
    data: {
        xacDinh: true
    }
});

var app22 = new Vue({
    el:'#app-22',
    data: {
        menus: ['Home', 'About', 'Blog', 'Contact', 'FAQ']
    }
});

var app23 = new Vue({
    el: '#app-23',
    data: {
        menus: ['Home', 'About', 'Blog', 'Contact', 'FAQ']
    }
});

var app24 = new Vue({
    el: '#app-24',
    data: {
        students: [
            { name: 'Cuong', age: 30, class: 'CNTT' },
            { name: 'Hoan', age: 10, class: 'CNTT' },
            { name: 'Hoang', age: 20, class: 'CNTT'},
            { name: 'Dai', age: 40, class: 'CNTT' },
            { name: 'Chuong', age: 50, class: 'CNTT'},
        ]
    }
});

var app25 = new Vue({
    el: '#app-25',
    data: {
        name: 'Cường'
    },
    methods: {
        greet: function (event) {
            alert('Hello ' + this.name + '!')
        }
    }
});

var app26 = new Vue({
    el: '#app-26',
    data: {
        counter: 0
    }
})

var app27 = new Vue({
    el: '#app-27',
    data: {
        name: 'Vue.js'
    },
    methods: {
        greet: function (event) {
            alert('Xin chào ' + this.name + '!')
            if (event) {
                alert(event.target.tagName)
            }
        }
    }
});

var app28 = new Vue({
    el: '#app-28',
    methods: {
        say: function (message) {
            alert(message)
        }
    }
});

var app29 = new Vue({
    el: '#app-29',
    data: {
        checkedNames: []
    }
})

var app30 = new Vue({
    el: '#app-30',
    data: {
        selected: ''
    }
});

var app31 = new Vue({
    el: '#app-31',
    data: {
        selected: '50k',
        options: [
            { text: 'Đọt bí xào tỏi', value: '50k' },
            { text: 'Canh bông điên điển', value: '30k' },
            { text: 'Lẩu nấm', value: '45k' }
        ]
    }
});

var app32 = new Vue({
    el: '#app-32',
    data: {
        checkedNames: []
    }
})
app.message = "Helle anh Cuong";
app2.message = "Tiêu đề";
app3.message = "";
app4.todos.push({ text: 'Them 4' });
app4.todos.push({ text: 'Them 5' });