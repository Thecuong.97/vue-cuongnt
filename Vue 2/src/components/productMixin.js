export const productMixin = {

    data: function () {
        return {
            products: ['iphone', 'samsung', 'xiaomi'],
            filterProduct: ''
        }
    },
    computed: {
        filterProducts() {
            return this.products.filter((product) => {
                return product.match(this.filterProduct);
            })
        }
    }
}